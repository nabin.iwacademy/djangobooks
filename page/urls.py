from django.urls import path
from . import views

urlpatterns = [
    path('', views.SignUpView.as_view(), name='signup'),
    path('validate_username/', views.validate_username, name='validate_username'),
]