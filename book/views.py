from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from .models import Book
from .forms import BookForm

def book_list(request):
    books = Book.objects.all()
    return render(request, 'book_list.html',{'books:books'})

def boo_create(request):
    form =BookForm()
    context = {'form': form}
    html_form = render_to_string('',
                                 context,
                                 request=request)
    return JsonResponse({'html_form': html_form})