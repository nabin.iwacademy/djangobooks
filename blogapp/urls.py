from django.urls import path, include
from . import views
urlpatterns = [
    path('', views.BlogListView.as_view(), name='bloghome'),
    path('post/<int:pk>/', views.BlogDetailView.as_view(), name='blog-details-home'),
    path('post/new/', views.BlogCreateView.as_view(), name='blog_create_home'),
    path('post/<int:pk>/edit/', views.BlogUpdateView.as_view(), name='post_edit'),
    path('post/<int:pk>/delete/', views.BlogDeleteView.as_view(), name='post_delete'),
]

